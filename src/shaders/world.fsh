
void main() {																								//The world-renderer

	vec2 uv = (gl_FragCoord.xy - vec2(0.5) * vec2(u_screen_width, u_screen_height)) / u_screen_height;		//Retrieving a centered screen-uv from -1 to 1 in both axes (xy).
	vec3 cr = vec3(0, 0, 1);																				//Defines a vector for the current camera ray pointing forward
	cr.xz *= rotate_vec2_by(-uv.x * (PI / (360.0 / u_camera_fov * 2.0)));									//Rotates the cam-ray according to the uv and specified field of view (y-axis)
	cr.yz *= rotate_vec2_by(-uv.y * (PI / (360.0 / u_camera_fov * 2.0)));									//The same around x-axis. These rotations creates a 3D view with perspective

	float render_result[5] = raymarch(u_camera_position, cr, u_max_depth_steps);							//Returns unlit scene color, depth and the amount of raymarcher steps before hitting surface
	vec3 col = vec3(render_result[0], render_result[1], render_result[2]);									//Making variable to hold scene color
	vec3 pwp = u_camera_position + cr * render_result[3];													//Current pixels 3D position in the world at hit-depth from camera.
	vec3 n = vec3(0, 0, 0);

	if (abs(length(u_camera_position - pwp)) < u_render_distance) {											//Checking if current depth is shorter than render distance.
		n = normal(pwp);																					//Calculating the normal vector for the surface at current pixel position
		col = sun(col, pwp, n);															//calculating diffuse lighting with shadow for current pixel.
		float f = 1.0 - dot(normalize(u_camera_position - pwp), n);											//Simple fresnel aproximation (about as simple as my light-model)
	}

	else {
		col = ambient_light();																				//Sending final color back to main.rs where glutin does stuff
	}

	//Post processing
	col = mix(col, ambient_light(), clamp(render_result[3] / u_render_distance, 0.0, 1.0));					//Making fog based on current render distance and ambient lighting (smooth_transition)
	//col = screen_noise(vec3(out_color.x, out_color.y, out_color.z), uv, vec2(u_screen_width, u_screen_height));
	col *= vignette(uv, 0.75);
	col = mix(col, vec3(render_result[3] / u_render_distance), 0.0);
	//col = mix(col, col / vec3(3.0), dot(col, vec3(1, 1, 1)));

	out_color = vec4(col.x, col.y, col.z, 0.0);
}
