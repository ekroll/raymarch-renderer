#version 450
#ifdef GL_FRAGMENT_PRECISION_HIGH
  precision highp float;
#else
  precision mediump float;
#endif

#define PI 3.14159
uniform float u_time;
float u_mouse_x = 0.0;
float u_mouse_y = 0.0;
uniform int u_screen_width;
uniform int u_screen_height;
uniform vec3 u_camera_position;
uniform float u_camera_fov;

uniform float u_render_distance;
uniform int u_max_depth_steps;
uniform int u_max_light_steps;
uniform float u_rayhit_margin;

uniform float u_planet_radius;
uniform float u_terrain_height;
uniform vec3 u_sun_color;
uniform vec3 u_ambient_light_color;
uniform float u_ambient_light_energy;

float day_cycle_length_seconds = 60.0;
out vec4 out_color;

float map(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

mat2 rotate_vec2_by(float angle) {
	float sine_of_angle = sin(angle);
	float cosine_of_angle = cos(angle);
	mat2 rotation_matrix = mat2(cosine_of_angle, -sine_of_angle, sine_of_angle, cosine_of_angle);
	return rotation_matrix;
}

float hash(vec2 p) {
  p = floor(p);
    return fract(sin(p.x * 10.0 + p.y * 657.0) * 567.0);
}

float seamless_noise_layer(vec2 p) {
	vec2 lv = smoothstep(0.0, 1.0, fract(p * 10.0));
  vec2 id = floor(p * 10.0);

  float bl = hash(id);
 	float br = hash(id + vec2(1.0, 0.0));
  float b = mix(bl, br, lv.x);

  float tl = hash(id + vec2(0.0, 1.0));
 	float tr = hash(id + vec2(1.0, 1.0));
  float t = mix(tl, tr, lv.x);
  return mix(b, t, lv.y);
}

float seamless_noise(vec2 p, float zoom, int iterations) {
	float noise = seamless_noise_layer(p / zoom);
  for (int i = 1; i < iterations; i++) {
    p *= rotate_vec2_by(float(i) / float(iterations) * 360.0);
    p += vec2(float(i) / float(iterations), 0.0);
    noise += seamless_noise_layer(p / zoom / float(i));
  }
  return noise;
}

///////////////////////////////Objects_and_shapes/////////////////////////////////
vec4 ground_plane(vec3 render_origin, float plane_height, vec3 color) {
	float dist = render_origin.y + plane_height * -1;
	return vec4(color.x, color.y, color.z, dist);
}

vec4 sphere(vec3 render_origin, vec3 sphere_position, float radius, vec3 color) {
  float dist = length(render_origin - sphere_position) - radius;
	return vec4(color.x, color.y, color.z, dist);
}

vec4 terrain_layer(vec3 render_origin, float terrain_scale, float terrain_height, float sand_height, vec3 beach_color, vec3 main_color) {
  float height = render_origin.y;
  terrain_height *= 2.0;
  height += seamless_noise_layer(render_origin.xz / terrain_scale) * terrain_height  *2.0;
		//height += seamless_noise(render_origin.xz, terrain_scale, 2) * terrain_height;

  float mix_factor = (abs(render_origin.y) - terrain_height / terrain_height - terrain_height + sand_height) / terrain_height * terrain_height;
  mix_factor = clamp(mix_factor, 0.0, 1.0);

  vec3 color = mix(main_color, beach_color, mix_factor);
	return vec4(color.x, color.y, color.z, height);
}

/////////////////////////////////The_world//////////////////////////////////
vec4 surface(vec3 current_position) {
  const int amount_of_objects = 3;
  float terrain_height = 1000.0;

vec4 distances[amount_of_objects] = { // Try to sort objects by which ones that will have the shortest distance to the camera over time
  terrain_layer(current_position * 0.85, 40000.0, terrain_height, 5.0, vec3(0.25, 0.35, 0.0), vec3(0.125, 0.25, 0)),
  ground_plane(current_position, -terrain_height * 2.0, vec3(0.0, 0.02, 0.2)),
  sphere(current_position, vec3(0.0, 1.85, 1.0), 0.25, vec3(1.0, 0.0, 0.0))
};

vec4 closest_surface = distances[0];
  for (int i = 1; i < amount_of_objects; i++) {
    if (distances[i].w < closest_surface.w) closest_surface = distances[i];
  }
	return closest_surface;
}

/////////////////////////////////More_useful_calculations///////////////////////////////
vec3 normal(vec3 current_position) {
	float distance_to_world_surface = surface(current_position).a;
  vec2 epsilon = vec2(u_rayhit_margin * 0.99, 0.0);

  vec3 raw_normal = distance_to_world_surface - vec3(
    surface(current_position - epsilon.xyy).w,
    surface(current_position - epsilon.yxy).w,
    surface(current_position - epsilon.yyx).w
  );
    	return normalize(raw_normal);
}

float[5] raymarch(vec3 origin, vec3 ray_direction, int max_steps) {
  float d = 0.0;
  int s = 0;
  vec3 cp;
  vec4 world_surface;

  for (int i = 0; i < max_steps; i++) {
  	cp = origin + ray_direction * d;
  	world_surface = surface(cp);
  	d += world_surface.w;
  	s += i;
  	if (abs(world_surface.w) < u_rayhit_margin || d >= u_render_distance) break;
  }

  float render_result[5] = {world_surface.x, world_surface.y, world_surface.z, d, float(s)};
  return render_result;
}

///////////////////////////////////////Lights//////////////////////////////////////
vec3 ambient_light() {
  vec3 light = u_ambient_light_color * (cos(u_time * PI / day_cycle_length_seconds * 2.0) + 1.0) / 2.0;
  return light;
}

vec3 sun(vec3 surface_color, vec3 pwp, vec3 n) {
  vec3 light_position = vec3(0, 0, 0);
  light_position.x = sin(u_time * PI / day_cycle_length_seconds * 2.0) * 2000;
  light_position.y = cos(u_time * PI / day_cycle_length_seconds * 2.0) * 2000;
  light_position.z = cos(u_time * PI / day_cycle_length_seconds * 2.0) * 2000;

	vec3 light_vector = normalize(light_position - vec3(0));
  float max_ray_length = length(vec3(0, 0, 0) - light_position);
  float shade_factor = dot(light_vector, n);
  shade_factor = clamp(shade_factor, 0.0, 1.0);
	float raymarch_result[5] = raymarch(pwp + n * u_rayhit_margin, light_vector, u_max_light_steps);

  vec3 light = surface_color * u_sun_color * 5.0 * clamp(cos(u_time * PI / day_cycle_length_seconds * 2.0), 0.0, 1.0);
  vec3 shade = surface_color * ambient_light();
  vec3 base_shading = mix(shade, light, clamp(shade_factor, 0.0 , 1.0));

  bool shadow = false;
  if (raymarch_result[3] < float(u_render_distance)) shadow = true;

	if (shadow) {
		surface_color = mix(shade, base_shading, clamp(raymarch_result[3] / max_ray_length, 0.0, 1.0));
	}

  else {
    surface_color = base_shading;
  }

	return surface_color;
}

//Effects//
float vignette(vec2 uv, float vignette_amount) {
  float dist = length(uv);
  dist *= vignette_amount;																//Calculating distance towards edge of screen																														//Decreasing vignette effect a bit
  float vignette = 1.0 - dist;
  vignette /= vignette_amount;
	vignette = clamp(vignette, 0.0, 1.0);																								//Clamping vignetteeffect between 0 and 1
  return vignette;
}

vec3 screen_noise(vec3 color, vec2 uv, vec2 resolution) {
  float screen_noise = hash(uv * resolution / vec2(512.0));
  screen_noise = map(screen_noise, 0.0, 1.0, 0.95, 1.05);
  color *= screen_noise;
  color = mix(color, vec3(0.5), 1.0 - screen_noise);
  color = mix(vec3(dot(vec3(1.0), color) * 0.33333), color, screen_noise);
  return color;
}
