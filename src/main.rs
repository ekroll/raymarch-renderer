#[macro_use] extern crate gfx;
extern crate gfx_window_glutin;
extern crate glutin;

use gfx::traits::FactoryExt;
use gfx::Device;
use gfx_window_glutin as gfx_glutin;
use glutin::WindowEvent::*;
use std::time::Instant;
use winit;
use std::f32::consts::PI;

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;

const RED: [f32; 4] = [1.0, 1.0, 1.0, 1.0];

gfx_defines! {
    vertex Vertex {
        pos: [f32; 2] = "a_pos",
    }

    pipeline graphics_pipeline {
        vbuf: gfx::VertexBuffer<Vertex> = (),

        //mouse_x:                gfx::Global<i32>        =   "u_mouse_x",
        //mouse_y:                gfx::Global<i32>        =   "u_mouse_y",
        time:                   gfx::Global<f32>        =   "u_time",
        res_width:              gfx::Global<i32>        =   "u_screen_width",
        res_height:             gfx::Global<i32>        =   "u_screen_height",

        cam_pos:                gfx::Global<[f32; 3]>   =   "u_camera_position",
        cam_fov:                gfx::Global<f32>        =   "u_camera_fov",

        render_dist:            gfx::Global<f32>        =   "u_render_distance",
        max_depth_steps:        gfx::Global<i32>        =   "u_max_depth_steps",
        max_light_steps:        gfx::Global<i32>        =   "u_max_light_steps",
        rayhit_margin:          gfx::Global<f32>        =   "u_rayhit_margin",

        planet_radius:          gfx::Global<f32>        =   "u_planet_radius",
        sun_col:                gfx::Global<[f32; 3]>   =   "u_sun_color",
        ambient_light_col:      gfx::Global<[f32; 3]>   =   "u_ambient_light_color",
        ambient_light_energy:   gfx::Global<f32>        =   "u_ambient_light_energy",

        out: gfx::RenderTarget<ColorFormat> = "out_color",
    }
}

const QUAD: [Vertex; 6] = [
    Vertex {pos: [ 1.0,  1.0]},
    Vertex {pos: [-1.0,  1.0]},
    Vertex {pos: [-1.0, -1.0]},

    Vertex {pos: [ 1.0,  1.0]},
    Vertex {pos: [-1.0, -1.0]},
    Vertex {pos: [ 1.0, -1.0]}
];

//#[shader_param(MyBatch)]
//struct Params {
//    u_camera_position: [f32, ..3]
//}


fn main() {

    //let mouse_x = 0;
    //let mouse_y = 0;

    //World stuff
    let planet_radius: f32 = 6371000.0;
    let terrain_height = 1000.0;
    let mut speed_kmh = 2000.0;


    //Rendering
    let screen_width = 1920 / 2 as u32;
    let screen_height = 1080 / 2 as u32;

    let monitor_width_cm = 52.0;
    let view_distance_cm = 26.0;

    let mut cam_fov: f32 = monitor_width_cm / 2.0 / view_distance_cm;
    cam_fov = (cam_fov.atan() / PI) * 360.0;

    let mut render_dist = (planet_radius + terrain_height).powf(2.0) - planet_radius.powf(2.0);
    render_dist = render_dist.sqrt();
    let max_depth_steps = 250;
    let max_light_steps = 85;
    let rayhit_margin = 0.01 * 2.0;

    let sun_col = [1.0, 0.95, 0.5];
    let ambient_light_col = [0.5, 0.75, 1.0];
    let ambient_light_energy = 0.75;

    //Stuff
    let mut spawn_position: [f32; 3] = [0.0, 1.85, 0.0];
    spawn_position[2] -= monitor_width_cm / 100.0;

    //Technical difficulties
    let mut frametime;
    let mut fps = 0.0;
    let mut time = 0.0;

/////////////////////////////////////////////////Boring///////////////////////////////////////////////////////////////////////////

    let event_loop = glutin::EventsLoop::new();
    let builder = glutin::WindowBuilder::new().with_title("Crazy Worlds".to_string()).with_dimensions(screen_width, screen_height).with_vsync();
    let (window, mut device, mut factory, main_color, mut main_depth) = gfx_glutin::init::<ColorFormat, DepthFormat>(builder, &event_loop);
    let mut encoder: gfx::Encoder<_, _> = factory.create_command_buffer().into();

    let shader_library = include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/src/shaders/shader_library.h"));
    let world_shader = include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/src/shaders/world.fsh"));

    let combined_power = format!("{lib}{shader}", //Since shaders are stupid when it comes to files, I have to manually fuse the files together as strings and convert it to stuff the pipeline can read
        lib=String::from_utf8_lossy(shader_library),
        shader=String::from_utf8_lossy(world_shader)
    );

    let frag_shader: &[u8] = combined_power.as_ref();

    let pso = factory.create_pipeline_simple(
        include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/src/shaders/screen_quad.vsh")),
        frag_shader,
        graphics_pipeline::new()
    ).unwrap();

    let (vertex_buffer, slice) = factory.create_vertex_buffer_with_slice(&QUAD, ());

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    let mut graphics_pipeline = graphics_pipeline::Data {
        vbuf: vertex_buffer,

        //mouse_x:                mouse_x,
        //mouse_y:                mouse_y,
        time:                   time,
        res_width:              screen_width as i32,
        res_height:             screen_height as i32,

        cam_pos:                spawn_position,
        cam_fov:                cam_fov,

        render_dist:            render_dist,
        max_depth_steps:        max_depth_steps,
        max_light_steps:        max_light_steps,
        rayhit_margin:          rayhit_margin,

        planet_radius:          planet_radius,
        sun_col:                sun_col,
        ambient_light_col:      ambient_light_col,
        ambient_light_energy:   ambient_light_energy,

        out: main_color
    };

    speed_kmh *= 3.6;
    println!("fov: {}", cam_fov);
    let new_time = Instant::now();

    let mut move_forward = false;
    let mut move_backwards = false;
    let mut move_left = false;
    let mut move_right = false;

    let mut run = true;
    while run {
        let new_frametime = Instant::now();
        event_loop.poll_events(|glutin::Event::WindowEvent{window_id: _, event}| {
            match event {
                KeyboardInput(glutin::ElementState::Pressed, _, Some(glutin::VirtualKeyCode::W), _) => move_forward = true,
                KeyboardInput(glutin::ElementState::Released, _, Some(glutin::VirtualKeyCode::W), _) => move_forward = false,

                KeyboardInput(glutin::ElementState::Pressed, _, Some(glutin::VirtualKeyCode::A), _) => move_left = true,
                KeyboardInput(glutin::ElementState::Released, _, Some(glutin::VirtualKeyCode::A), _) => move_left = false,

                KeyboardInput(glutin::ElementState::Pressed, _, Some(glutin::VirtualKeyCode::S), _) => move_backwards = true,
                KeyboardInput(glutin::ElementState::Released, _, Some(glutin::VirtualKeyCode::S), _) => move_backwards = false,

                KeyboardInput(glutin::ElementState::Pressed, _, Some(glutin::VirtualKeyCode::D), _) => move_right = true,
                KeyboardInput(glutin::ElementState::Released, _, Some(glutin::VirtualKeyCode::D), _) => move_right = false,

                KeyboardInput(glutin::ElementState::Pressed, _, Some(glutin::VirtualKeyCode::Space), _) => graphics_pipeline.cam_pos[1] += speed_kmh / fps,
                KeyboardInput(glutin::ElementState::Pressed, _, _, winit::ModifiersState{ctrl: true, shift:false, alt: false, logo: false}) => graphics_pipeline.cam_pos[1] -= speed_kmh / fps,
                KeyboardInput(_, _, Some(glutin::VirtualKeyCode::Escape), _) => run = false,
                Resized(_, _) => {
                    gfx_glutin::update_views(&window, &mut graphics_pipeline.out, &mut main_depth)
                }, _ => (),
            }

            if move_forward {
                graphics_pipeline.cam_pos[2] += speed_kmh / fps;
            }

            if move_backwards {
                graphics_pipeline.cam_pos[2] -= speed_kmh / fps;
            }

            if move_left {
                graphics_pipeline.cam_pos[0] -= speed_kmh / fps;
            }

            if move_right {
                graphics_pipeline.cam_pos[0] += speed_kmh / fps;
            }
        });

        graphics_pipeline.time = time;

        encoder.clear(&graphics_pipeline.out, RED);
        encoder.draw(&slice, &pso, &graphics_pipeline);
        encoder.flush(&mut device);
        window.swap_buffers().unwrap();
        device.cleanup();

        time = new_time.elapsed().as_millis() as f32;
        time /= 1000.0;
        frametime = new_frametime.elapsed().as_millis();
        fps = 1000.0 / frametime as f32;
        println!("frametime: {:?}ms | fps: {} | cam_pos: {:?}", frametime, fps as u32, graphics_pipeline.cam_pos);
    }
}
